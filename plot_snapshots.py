from radamesh_py import RadameshPy as RP
from my_visualization import MyVisualization as MV
import os
import glob
import numpy as np




def plotting(root, dir, file):
    chombo = RP(root + dir + file)
    chombo.load()

    box0 = chombo.levels['level_0']['boxes'][0]
    data = chombo.levels['level_0']['data']

    len = [box0[3] + 1, box0[4] + 1, box0[5] + 1]
    dim = len[0] * len[1] * len[2]

    den = data[int(len[1] / 2 * len[0]):int(len[1] / 2) * len[0] + len[0]]
    temp = data[dim + int(len[1] / 2 * len[0]):dim + int(len[1] / 2) * len[0] + len[0]]
    fHI = data[2*dim + int(len[1] / 2 * len[0]):2*dim + int(len[1] / 2) * len[0] + len[0]]
    fHeI = data[3*dim + int(len[1] / 2 * len[0]):3*dim + int(len[1] / 2) * len[0] + len[0]]
    fHeII = data[4*dim + int(len[1] / 2 * len[0]):4*dim + int(len[1] / 2) * len[0] + len[0]]

    xlabel = r"X [$\frac{1}{40}$ kpc]"

    if not os.path.exists(root + 'plots'):
        os.makedirs(root + 'plots')

    vis = MV()

    vis.plt.cla()

    ext = ("%5.3e" % chombo.attrs["time"]) + ("--%d" % chombo.attrs["iteration"]) + ".png"

    vis.plot(np.linspace(0.0, 1.0, len[0]), temp, xlabel=xlabel, ylabel='T [K]', yscale='log')
    vis.save(root + 'plots/' + file + "-temp-" + ext)
    vis.plt.cla()

    vis.plot(np.linspace(0.0, 1.0, len[0]), fHI, ymin=1.0e-9, ymax=1.05, xlabel=xlabel, ylabel='Ionization States', yscale='log', color='#ff4843')
    vis.plot(np.linspace(0.0, 1.0, len[0]), fHeI, ymin=1.0e-9, ymax=1.05, xlabel=xlabel, ylabel='Ionization States', yscale='log', color='#328bdc')
    vis.plot(np.linspace(0.0, 1.0, len[0]), fHeII, ymin=1.0e-9, ymax=1.05, xlabel=xlabel, ylabel='Ionization States', yscale='log', color='#5fae5b')

    vis.save(root + 'plots/' + file + "-ionization-state-" + ext)
    vis.plt.cla()

    del vis


if __name__ == "__main__":
    def sortKeyFunc(s):
        return int(os.path.basename(s)[-4:])

    dirs = glob.glob("RADAMESH*")

    for dir in dirs:
        files = glob.glob(dir + '/output/*.ts*')
        files.sort(key=sortKeyFunc)

        for file in files:
            print(file)
            plotting(dir + '/', 'output/', os.path.basename(file))
