from radamesh_py import RadameshPy as RP
from my_visualization import MyVisualization as MV
import math
import os
import glob
import numpy as np


kb_eV = 8.6173303e-5 # eV K^-1
m_H = 1.6737236e-24 # g


def q1_2p(T):
    # From Giovanardi et al. 1987
    Gamma = 3.162e-1 + 1.472e-5 * T - 8.275e-12 * T**2 - 8.794e-19 * T**3
    # Gamma = 3.435e-1 + 1.297e-5 * T + 2.178e-12 * T**2 + 7.928e-17 * T**3
    return (8.629e-6 * Gamma * math.exp(-12.1/(kb_eV * T)) / (math.sqrt(T) * 2)) # cm^3 s^-1


def q1_3s(T):
    # From Giovanardi et al. 1987
    Gamma = 3.337e-2 + 2.223e-7 * T - 2.794e-13 * T**2 + 1.516e-19 * T**3
    # Gamma = 6.250e-2 - 1.299e-6 * T + 2.666e-11 * T**2 - 1.596e-16 * T**3
    return (8.629e-6 * Gamma * math.exp(-12.6/(kb_eV * T)) / (math.sqrt(T) * 8)) # cm^3 s^-1


def q1_3d(T):
    # From Giovanardi et al. 1987
    Gamma = 5.051e-2 + 7.876e-7 * T - 2.072e-12 * T**2 + 1.902e-18 * T**3
    # Gamma = 5.030e-2 + 7.514e-7 * T - 2.826e-13 * T**2 + 1.098e-17 * T**3
    return (8.629e-6 * Gamma * math.exp(-12.6/(kb_eV * T)) / (math.sqrt(T) * 8)) # cm^3 s^-1


def alpha_rec(T):
    # From Hui & Genedin 1996
    lam = 2 * 157807 / T
    return (1.269e-13 * lam**1.503 / (1 + (lam/0.522)**0.470)**1.923) # cm^3 s^-1


def emission_fraction(path):
    chombo = RP(path)
    chombo.load()

    box0 = chombo.levels['level_0']['boxes'][0]
    data = chombo.levels['level_0']['data']

    len = [box0[3] + 1, box0[4] + 1, box0[5] + 1]
    dim = len[0] * len[1] * len[2]

    time = chombo.attrs["time"]

    den = data[int(len[1] / 2 * len[0]):int(len[1] / 2) * len[0] + len[0]]
    temp = data[dim + int(len[1] / 2 * len[0]):dim + int(len[1] / 2) * len[0] + len[0]]
    fHI = data[2*dim + int(len[1] / 2 * len[0]):2*dim + int(len[1] / 2) * len[0] + len[0]]
    fHeI = data[3*dim + int(len[1] / 2 * len[0]):3*dim + int(len[1] / 2) * len[0] + len[0]]
    fHeII = data[4*dim + int(len[1] / 2 * len[0]):4*dim + int(len[1] / 2) * len[0] + len[0]]

    rec = 0.0
    col = 0.0

    for r,t,hi,hei,heii in zip(den, temp, fHI, fHeI, fHeII):
        hii = 1.0 - hi
        heiii = 1.0 - hei - heii

        epsilon_rec = 0.686 - 0.106 * math.log10(t/1.e4) - 0.009 * (t/1.e4)**-0.44

        alpha_eff = epsilon_rec * alpha_rec(t)
        q_eff = q1_2p(t) + q1_3s(t) + q1_3d(t)

        # one_over_mu =  .75 * (1.0 + hii) / 1.00794 + .25 * (1.0 + heii + 2 * heiii) / 4.002602
        one_over_mu = 0.75 / 1.00794 + 0.25 / 4.002602

        n_HI = hi * .75 * r * one_over_mu / m_H
        n_HII = hii * .75 * r * one_over_mu / m_H
        n_e = (hii * .75 + (heii + 2 * heiii) * .25) * r * one_over_mu / m_H

        rec += alpha_eff * n_HII * n_e
        col += q_eff * n_HI * n_e

    del chombo

    return time, rec, col


if __name__ == "__main__":
    def sortKeyFunc(s):
        return int(os.path.basename(s)[-4:])


    dirs = glob.glob("RADAMESH*")


    for dir in dirs:
        print(dir)

        times = []
        rec = []
        col = []

        vis = MV()

        files = glob.glob(dir + '/output/*.ts*')
        files.sort(key=sortKeyFunc)

        for file in files:
            t, r, c = emission_fraction(file)
            times.append(t * 1.0e6)
            rec.append(r / (r+c))
            col.append(c / (r+c))

        vis.plt.cla()
        vis.plot(times, rec, xlabel="t [yr]", ylabel="Relative fractions", xmin=0.0, xmax=1000, ymin=1.0e-4, ymax=1.05, xscale='log', yscale='log', color="#ff4843")
        vis.plot(times, col, xlabel="t [yr]", ylabel="Relative fractions", xmin=0.0, xmax=1000, ymin=1.0e-4, ymax=1.05, xscale='log', yscale='log', color="#328bdc")

        vis.save(dir + '/' + dir + '--relative_fractions.png')

        del vis
